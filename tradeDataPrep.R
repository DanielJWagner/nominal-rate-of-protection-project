# script to clean trade data and calculate import weights
# splitting this off because the loop is going to take something around 3 hours to run with commodities
# have since optimized this, but need to do something about currency, data is anchored to current dollar value but
# I've spread out my downloads

#emptying environment
rm(list = ls()) 

# loading packages
library(data.table)
# adding dplyr for count function
#install.packages("dplyr")
library(dplyr)


# loading in UN comtrade data
load("tradeData.RData")
tradeData <- unique(tradeData)
load("PSE.RData")




# now need to construct average import subsidy and distance variables
# can't think of how to do this with vectorization so doing it in loop
# need to think carefully about how to do this, merge in subsidy data to the trade data and then
# us that to calculate import weights?  But this gets into the filling 0s issue again
# at least need import value to calculate domestic share, but have trade data for kilograms in addition to value
# need to convert to metric tons

# while I have production data- can use faostat prices to get a crude estimate of production value
# have world trade data, don't need to sum imports- have more accurate measure since imports from the world
# should be accurate for tax purposes and not have issues from missing observations


# should I create a column of trade vectors for normal data?
# not going to bother with that yet



# need to rename crops for ease of use, be sure to check commodity codes to make sure I have the correct one

# rice is already just called rice
# need to move most of my renaming from other scripts to here, mostly consolidating crops that have multiple names
# listed

tradeData[cmdDescE == "Wheat and meslin"]$cmdDescE <- "Wheat"
# Barley and Rye also already have their names simplified
tradeData[cmdDescE == "Maize (corn)"]$cmdDescE <- "Maize"
# faostat has barley production data and Maize



# checking country names in existing data & trade data


# Russia, South Korea, Syria, Sudan, Tanzania, Venezuela, Taiwan, Ethiopia, Iran, Liechtenstein,
# Moldova, Macedonia, Vietnam, and the United States not matched initially
tradeData[rtTitle == "Russian Federation"]$rtTitle <- "Russia"

tradeData[rtTitle == "Rep. of Korea"]$rtTitle <- "Korea, South"
tradeData[rtTitle == "Viet Nam"]$rtTitle <- "Vietnam"
tradeData[rtTitle == "USA"]$rtTitle <- "United States"
tradeData[rtTitle == "United Rep. of Tanzania"]$rtTitle <- "Tanzania"
tradeData[rtTitle == "Bolivia (Plurinational State of)"]$rtTitle <- "Bolivia"


# checking crop names
# need to rename crops for ease of use
#tradeData[cmdDescE == "Rice"]$cmdDescE <- "Rice, paddy"
#"Rice, paddy is causing a lot of problems




# now need to construct average import subsidy and distance variables

# killing trade value being a factor, I don't know why it was like this originally
tradeData$TradeValue <- as.numeric(as.character(tradeData$TradeValue))

# to speed up loop could create an index of existing entries and simply walk down them?
importValues <- tradeData[ptTitle == "World" & rgDesc == "Import"]
# now need to make an index of countries, years, and commodities to walk down
# can just subset this dataset

importValues <- importValues[, c("yr", "rtTitle", "cmdDescE", "TradeValue"), with = FALSE]
setnames(importValues, "TradeValue", "ImportValue")

tradeData <- merge(importValues, tradeData, by = c("yr", "rtTitle", "cmdDescE"),  all.x = TRUE)
tradeData <- unique(tradeData)



# getting error
"1: In `[<-.data.table`(x, j = name, value = value) :
  Coerced 'integer' RHS to 'double' to match the column's type. Either change the target column ['ImportValue'] to 
'integer' first (by creating a new 'integer' vector length 34 (nrows of entire table) and assign that; i.e. 'replace' 
column), or coerce RHS to 'double' (e.g. 1L, NA_[real|integer]_, as.*, etc) to make your intent clear and for 
speed. Or, set the column type correctly up front when you create the table and stick to it, please."
# my call seems to be returing multiple entries, which shouldn't be the case?

# with base value, can now create weight for import value, 
tradeData[, importWeight:= TradeValue/ImportValue]
#import weights are very small, need to remember why I set it up this way
# is trade value in a different scale than the import value?
summary(tradeData)
#importWeight is giving a different mean from the actual data and it isn't clear why
temp <- tradeData$importWeight
summary(temp)
quantile(temp)
# summary is just rounding the 1st and 3rd quartiles to 0

temp <- tradeData[rgDesc == "Import"]
quantile(temp$importWeight)





setnames(tradeData, "rtTitle", "Country_Name")
setnames(tradeData, "cmdDescE", "Commodity_Description")
setnames(tradeData, "yr", "Market_Year")

# renaming commodities to improve matches
tradeData[Commodity_Description == "Apples, fresh" |
            Commodity_Description == "Fruit, edible; apples, fresh"]$Commodity_Description <- "Apples"
tradeData[Commodity_Description == "Palm oil and its fractions"]$Commodity_Description <- "Oil, palm"
tradeData[Commodity_Description == "Potatoes, fresh or chilled" | 
               Commodity_Description == "Potatoes, fresh or chilled."]$Commodity_Description <- "Potatoes"

tradeData[Commodity_Description == "Meat of swine, fresh, chilled or frozen" | 
               Commodity_Description == "Meat of swine, fresh, chilled or frozen." |
               Commodity_Description == "Meat of swine; fresh, chilled or frozen"]$Commodity_Description <- "Pork"
# apparently I have no production data on pork, but there are significant numbers of observations under each
# different spelling

tradeData[Commodity_Description == "Tomatoes, fresh or chilled." |
               Commodity_Description == "Tomatoes, fresh or chilled" |
            Commodity_Description == "Tomatoes; fresh or chilled"]$Commodity_Description <- "Tomatoes"
tradeData[Commodity_Description == "Barley."]$Commodity_Description <- "Barley"


# test has commodity named "Potatoes"
tradeData[Commodity_Description == "Potatoes; fresh or chilled"]$Commodity_Description <- "Potatoes"

# consolidating some different spellings of rice that don't matter for our purposes
tradeData[Commodity_Description == "Rice, semi-milled or wholly milled"]$Commodity_Description <- "Rice"

tradeData[Commodity_Description == "Palm oil and its fractions, not chemically modified"]$Commodity_Description <- "Oil, palm"
tradeData[Commodity_Description == "Palm oil and its fractions; whether or not refined, but not chemically modified"]$Commodity_Description <- "Oil, palm"
tradeData[Commodity_Description == "Palm oil and its fractions, whether or not refined, but not chemically modified" |
            Commodity_Description == "Palm oil and its fractions, whether or not refined, but not chemically modified."]$Commodity_Description <- "Oil, palm"

tradeData[Commodity_Description == "Rye."]$Commodity_Description <- "Rye"
tradeData[Commodity_Description == "Oats."]$Commodity_Description <- "Oats"
tradeData[Commodity_Description == "Rice."]$Commodity_Description <- "Rice"

# correct soybeans
tradeData[Commodity_Description == "Soya beans"]$Commodity_Description <- "Soybeans"
tradeData[Commodity_Description == "Soya beans, whether or not broken"]$Commodity_Description <- "Soybeans"
tradeData[Commodity_Description == "Soya beans, whether or not broken."]$Commodity_Description <- "Soybeans"

# consolidating avocados
tradeData[Commodity_Description == "Avocados, fresh/dried"]$Commodity_Description <- "Avocados"
tradeData[Commodity_Description == "Avocados, fresh or dried"]$Commodity_Description <- "Avocados"

# consolidating Maize which has more than one spelling
tradeData[Commodity_Description == "Maize (corn)."]$Commodity_Description <- "Maize"

# Consolidating Almonds
tradeData[Commodity_Description == "Almonds, shelled"]$Commodity_Description <- "Almonds"
# not doing any more because I don't have almond data

# Doing flowers here
tradeData[Commodity_Description == "Cut flowers and flower buds of a kind suitable for bouquets or for ornamental purposes, fresh, dried, dyed, bleached, impregnated or otherwise prepared." |
            Commodity_Description == "Cut flowers, dried flowers for bouquets, etc,"]$Commodity_Description <- "Flowers"
# I think I want to simplify the subsidy plants and flowers to just flowers

# Consolidating coffee
tradeData[Commodity_Description == "Coffee, whether or not roasted or decaffeinated"]$Commodity_Description <- "Coffee"
# there are more categories involving coffee but they also include husks or coffee substitutes so I don't think
# I should consolidate them just yet


# now eggs, normally think of this as chicken eggs and trade data doesn't specify but I'm not sure it matters
# will consolidate for now
tradeData[Commodity_Description == "Birds' eggs, in shell, fresh, preserved or cooked." |
            Commodity_Description == "Birds eggs, in shell, fresh, preserved or cooked" |
            Commodity_Description == "Birds' eggs, in shell; fresh, preserved or cooked"]$Commodity_Description <- "Eggs"

# Colza is rapeseed
tradeData[Commodity_Description == "Rape or colza seeds, whether or not broken." |
            Commodity_Description == "Rape or colza seeds; whether or not broken" |
            Commodity_Description == "Rape or colza seeds" ]$Commodity_Description <- "Rapeseed"


# need to add oils and figure out a way to aportion subsidies to them, probably duplicate the subsidies
# for the crop and save them as subsidies for the oils as well

# consolidating milk definitions- all of this is milk and cream, can I use this?
# just going to mush it together at least for now
tradeData[Commodity_Description == "Milk and cream, neither concentrated nor sweetened" |
            Commodity_Description == "Milk and cream; not concentrated, not containing added sugar or other sweetening matter" |
            Commodity_Description == "Milk and cream, not concentrated nor containing added sugar or other sweetening matter." |
            Commodity_Description == "Milk and cream, not concentrated nor containing added sugar"]$Commodity_Description <- "Milk"


# more rice consolidation
tradeData[Commodity_Description == 
            "Cereals; rice, semi-milled or wholly milled, whether or not polished or glazed" |
            Commodity_Description == "Semi-milled/wholly milled rice, whether or not polished/glazed"|
            Commodity_Description == "Semi-milled/wholly milled rice, whether/not polished/glazed" |
          Commodity_Description == "Semi-milled/wholly milled rice, whether or not polished/glazed"]$Commodity_Description <- "Rice"

# consolidating chicken
tradeData[Commodity_Description == "Cuts & edible offal of species Gallus domesticus, fresh/chilled" |
            Commodity_Description == "Meat and edible offal; of fowls of the species Gallus domesticus, cuts and offal, fresh or chilled"]$Commodity_Description <- "Chicken"

# consolidating sunflower seeds
tradeData[Commodity_Description == "Sunflower seeds; whether or not broken" |
            Commodity_Description == "Sunflower seeds, whether or not broken."]$Commodity_Description <- "Sunflower seeds"

# consolidating flowers
tradeData[Commodity_Description == "Flowers; cut flowers and flower buds of a kind suitable for bouquets or for ornamental purposes, fresh, dried, dyed, bleached, impregnated or otherwise prepared" |
            Commodity_Description == "Cut flowers and flower buds of a kind suitable for bouquets"]$Commodity_Description <- "Flowers"

# consolidating pineapples
tradeData[Commodity_Description == "Fruit, edible; pineapples, fresh or dried" | Commodity_Description == "Pineapples, fresh/dried" |
          Commodity_Description == "Pineapples, fresh or dried"]$Commodity_Description <- "Pineapples"

# consolidating grapes
tradeData[Commodity_Description == "Grapes; fresh or dried" | Commodity_Description == "Grapes, fresh or dried." |
            Commodity_Description == "Grapes, fresh or dried"]$Commodity_Description <- "Grapes"

# consolidating tobacco
tradeData[Commodity_Description == "Cigarette or pipe tobacco and tobacco substitute mixe" |
            Commodity_Description == "Smoking tobacco, whether/not containing tobacco substitutes in any proportion" |
            Commodity_Description == "Smoking tobacco, whether or not cont. tobacco substitutes in any proportion ..." |
            Commodity_Description == "Cigarette or pipe tobacco and tobacco substitute mixes"]$Commodity_Description <- "Tobacco"

# mangoes, mangosteens, and guavas are lumped together for essentially no reason?  probably because COMTRADE
# doesn't report at the 10 digit level, labeling it all mangoes for now and will drop in a robustness check regression
tradeData[Commodity_Description == "Fruit, edible; guavas, mangoes and mangosteens, fresh or dried" |
            Commodity_Description == "Guavas, mangoes & mangosteens, fresh/dried" | 
            Commodity_Description == "Guavas, mangoes and mangosteens, fresh or dried"]$Commodity_Description <- "Mango"

# consolidating flours
tradeData[Commodity_Description == "Potato flour, meal, flakes, etc" | Commodity_Description ==
            "Flour, meal, powder, flakes, granules and pellets of potatoes." | Commodity_Description ==
            "Flour, meal, powder, flakes, granules and pellets of potatoes"]$Commodity_Description <-"PotatoFlour"

tradeData[Commodity_Description == "Wheat or meslin flour." | Commodity_Description ==
            "Wheat or meslin flour"]$Commodity_Description <- "WheatFlour"

# need to consolidate oils
tradeData[Commodity_Description == "Maize (corn) oil, crude" | Commodity_Description == "Maize (corn) oil, other than crude, & fractions thereof , whether or not re ... "
          | Commodity_Description == "Maize (corn) oil, other than crude, & fractions thereof , whether/not refined but not chemically modified"
          | Commodity_Description == "Maize oil crude" |Commodity_Description == "Maize oil, fractions, refined not chemically modified"
          | Commodity_Description == "Vegetable oils; maize (corn) oil and its fractions, crude, not chemically modified"  |
            Commodity_Description == 
            "Vegetable oils; maize (corn) oil and its fractions, other than crude, whether or not refined, but not chemically modified"
          ]$Commodity_Description <- "MaizeOil"
# need to collect more specific oils if possible

# consolidating Soybean Oil
tradeData[Commodity_Description == "Soya-bean oil and its fractions, whether or not refined, but not chemically modified."|
            Commodity_Description == "Soya-bean oil and its fractions" |
            Commodity_Description == "Soya-bean oil, fractions, not chemically modified" |
            Commodity_Description == "Soya-bean oil and its fractions; whether or not refined, but not chemically modified"
          ]$Commodity_Description <- "SoybeanOil"

# consolidating soybean oil cake and residue
tradeData[Commodity_Description == "Soya-bean oil-cake and other solid residues" | 
            Commodity_Description == "Oil-cake and other solid residues; whether or not ground or in the form of pellets, resulting from the extraction of soya-bean oil"|
            Commodity_Description == "Oil-cake and other solid residues, whether or not ground or in the form of pellets, resulting from the extraction of soyabean oil."
            ]$Commodity_Description <- "Soybean oil cake and solid residue"

# consolidating colza and related oils
tradeData[Commodity_Description == "Rape, colza, or mustard oil" | 
            Commodity_Description == "Rape, colza, mustard oil, fractions, simply refined" |
            Commodity_Description == "Rape, colza or mustard oil and fractions thereof, whether or not refined, but not chemically modified." |
            Commodity_Description == "Rape, colza or mustard oil and their fractions; whether or not refined, but not chemically modified" 
            | Commodity_Description == "Rape, colza or mustard oil" 
            ]$Commodity_Description <- "Rape, colza, or mustard oil"

# Colza is 42 to 43% oil, found writeup from Canola council of canada

# I think I also need to consolidate wheat?  If I did it above doing more here, missing a lot of wheat observations
# in the middle of the period and I think maybe there was a conventions shift to labeling things as wheat and meslin

tradeData[Commodity_Description == "Cereals; wheat and meslin, other than durum wheat, other than seed" |
            Commodity_Description == "Wheat and meslin."]$Commodity_Description <- "Wheat"

unique(tradeData$Commodity_Description)

# didn't match trade data and PSE data on country names which may be borking things
intersect(unique(tradeData$Country_Name), unique(PSE$Country_Name))
# says Russia is in both datasets but there are no observations of countries trading with Russia?
intersect(unique(tradeData$ptTitle), unique(PSE$Country_Name))
# Russia is not in the overlap in ptTitle 
setdiff(unique(tradeData$ptTitle), unique(PSE$Country_Name))
# USA has been left out, as has Russian Federation
setdiff(unique(PSE$Country_Name), unique(tradeData$ptTitle))


# correcting ptTitle names
tradeData[ptTitle == "USA"]$ptTitle <- "United States"
tradeData[ptTitle == "Russian Federation"]$ptTitle <- "Russia"
# also losing South Korea, Vietnam, and Czech Republic
tradeData[ptTitle == "Rep. of Korea"]$ptTitle <- "Korea, South"
tradeData[ptTitle == "Viet Nam"]$ptTitle <- "Vietnam"
tradeData[ptTitle == "Czechia"]$ptTitle <- "Czech Republic"


unique(PSE$Commodity_Description)
intersect(unique(tradeData$Commodity_Description), unique(PSE$Commodity_Description))
setdiff(unique(tradeData$Commodity_Description), unique(PSE$Commodity_Description))
setdiff(unique(PSE$Commodity_Description), unique(tradeData$Commodity_Description))



# creating trade exposure weights, I think this can work if I calculate the value for every trade flow
# and then create a sum by country/commodity/year and split off a smaller dataset, delete all columns except for
# the country/year/commodity/exposure and then merge it back into the main dataset
# might need to move this back to before creating own share variable



# Might need to fill this with NAs instead so subsidies aren't assumed to be 0
# tradeData$subsidyExposure <- NA Actually I think this variable wasn't being used
# creating a new dataset so that any mistakes don't affect earlier code with PSE
exporterSubsidy <- PSE
setnames(exporterSubsidy, "Country_Name", "ptTitle")
# is this bleeding back to PSE? need to check on that


# merge works ok, need to be careful to not apply this to export flows
tradeData <- merge(tradeData, exporterSubsidy, by = c("ptTitle", "Market_Year", "Commodity_Description"),
                   all.x = TRUE)  # need to weave in EU subsidies here, but have to do it by year of accession 

tradeData$weightedImportSubsidy <- NA
temp <- tradeData[rgDesc == "Import"]$importWeight * tradeData[rgDesc == "Import"]$`Subsidy%`
# in this case subsidy% is the amount of subsidy from the exporter
tradeData[rgDesc == "Import"]$weightedImportSubsidy <- temp  
# this multiplication isn't working, need to figure out why
# is it because the import weights are still borked?
# getting false readings from exports
quantile(tradeData$weightedImportSubsidy, na.rm = TRUE)
# numbers for this are not weird, PSE is -100 to 100 so makes sense


# preparing to create FPSE with constant weights, using average trade value over the entire period as a weight
# the trick is I need to not double count world imports
temp <- tradeData[rgDesc == "Import" & ptTitle == "World"]
totalImports <- temp[, sum(TradeValue), by = list(Country_Name, Commodity_Description)]
# have created total imported value, now need to divide this by years observed
# also need to merge totalImports variable back in, with is the last variable v1 of the above dataset
importYears <- count(temp, Country_Name, Commodity_Description)
temp <- merge(totalImports, importYears, by = c("Country_Name", "Commodity_Description"))
names(temp) <- c("Country_Name", "Commodity_Description", "totalImports", "yearsObserved")
temp$constantWeight <- temp$totalImports/temp$yearsObserved


# dropping unneeded variables before merging back in
temp <- temp[, c("Country_Name", "Commodity_Description", "constantWeight"), with = FALSE]

tradeData <- merge(tradeData, temp, by = c("Country_Name", "Commodity_Description"), all.x = TRUE)

# misunderstood constant weights, need to not only have a constant denominator but a constant share for each country
# need to find average share of trade value in a crop from each trade partner over time
# can sum trade value for a country-partner-crop pairing, average and divide by constant weight
# and append it to each observation for that country crop pairing
shareWeights <- tradeData[, mean(TradeValue), by = list(Country_Name, Commodity_Description, ptTitle)]
# getting Areas, nes as a trade partner and I don't know what that means
# it means areas not elsewhere specified, seems to be a lump category for countries that aren't tracked individually
# which can't be many of them if this includes places like Cyprus?  Or is it a dumping ground for incomplete data
# but otherwise seems to be working

# renaming generated value
names(shareWeights) <- c("Country_Name", "Commodity_Description", "ptTitle", "averageTradeValue")

# dropping unneeded variables and merging back in
shareWeights <- shareWeights[, c("Country_Name", "Commodity_Description", "ptTitle", "averageTradeValue")]

tradeData <- merge(tradeData, shareWeights, by = c("Country_Name", "Commodity_Description", "ptTitle"), all.x = TRUE)
# merge is producing .x and .y average trade value, need to review logic on merges and find out why
# bug is not reproduceable on my office PC, and is now not occuring at all, maybe have been from me running the code twice without
# clearing

# making a denominator with no NAs in it
# can't include world trade or that is double counting, also dropping NAs in subsidy data which are the missing entries
temp <- tradeData[ptTitle != "World" & is.na(`Subsidy%`) == TRUE , sum(TradeValue), by = list(Country_Name, Commodity_Description)]
setnames(temp, "V1", "constantWeightNoNA")

tradeData <- merge(tradeData, temp, by = c("Country_Name", "Commodity_Description"), all.x = TRUE)
# this works pretty well, only lose a handful of observations

# calculating constant FPSE, first need to create weights
tradeData[, oldConstantImportWeight:= TradeValue/constantWeight]
# now calculating exposure for each trade flow
tradeData[,oldConstantWeightedImportSubsidy := oldConstantImportWeight*`Subsidy%`]
tradeData[,oldLaggedConstantWeightedImportSubsidy := oldConstantImportWeight*LaggedOwnSubsidy]

tradeData[, constantWeightedImportSubsidy := averageTradeValue*`Subsidy%`/constantWeight ]
# lots of NAs but so does the old variable, seems this is due to holes in the Domestic subsidy
# Hamilton/Heins asked about this, missing data is treated as NAs and not 0s
tradeData[, laggedConstantWeightedImportSubsidy := averageTradeValue*LaggedOwnSubsidy/constantWeight]
tradeData[, constantWeightedImportSubsidyNoNA := averageTradeValue*`Subsidy%`/constantWeightNoNA]

temp <- aggregate(oldConstantWeightedImportSubsidy ~ Country_Name + Market_Year + Commodity_Description, tradeData, sum)
setnames(temp, "oldConstantWeightedImportSubsidy", "oldFPSEconstant" )

quantile(temp$oldFPSEconstant, na.rm = TRUE)
# quantiles with constant weights are drastically different
# but lose almost half of my observations compared to standard FPSE, but are these the NA observations?
sum(is.na(temp$oldFPSEconstant))
# no NAs in this data, but also no NAs in standard FPSE, am I missing world observations for some years?
# but that should have been included in every datapull
plot(temp$Market_Year, temp$oldFPSEconstant)
# getting ~20 observations over 10,000; and in later years- growth of trade over time has made subsidy exposure
# very large in some cases, may need to retitle this to relative subsidy exposure

# merging back in, may need to evaluate removing outliers
tradeData <- merge(tradeData, temp, by = c("Country_Name", "Market_Year", "Commodity_Description"), all.x = TRUE)
summary(tradeData$oldFPSEconstant)

# creating properly weighted FPSE variable and merging in
temp <- aggregate(constantWeightedImportSubsidy ~ Country_Name + Market_Year + Commodity_Description, tradeData, sum)
setnames(temp, "constantWeightedImportSubsidy", "FPSEconstant" )
tradeData <- merge(tradeData, temp, by = c("Country_Name", "Market_Year", "Commodity_Description"), all.x = TRUE)

# summing no NA measure
temp <- aggregate(constantWeightedImportSubsidyNoNA ~ Country_Name + Market_Year + Commodity_Description, tradeData, sum)
setnames(temp, "constantWeightedImportSubsidyNoNA", "FPSEconstantNoNA" )
tradeData <- merge(tradeData, temp, by = c("Country_Name", "Market_Year", "Commodity_Description"), all.x = TRUE)



# basic descriptive statistics
quantile(tradeData$FPSEconstant, na.rm = TRUE)
# still has outliers
plot(tradeData$Market_Year, tradeData$FPSEconstant)
# outliers make this mostly flat
plot(tradeData[FPSEconstant < 2000 & FPSEconstant > -500]$Market_Year, tradeData[FPSEconstant < 2000 & FPSEconstant > -500]$FPSEconstant)

# merging in properly weighted lagged FPSE
temp <- aggregate(laggedConstantWeightedImportSubsidy ~ Country_Name + Market_Year + Commodity_Description, tradeData, sum)
setnames(temp, "laggedConstantWeightedImportSubsidy", "laggedFPSEconstant" )
tradeData <- merge(tradeData, temp, by = c("Country_Name", "Market_Year", "Commodity_Description"), all.x = TRUE)

temp <- aggregate(oldLaggedConstantWeightedImportSubsidy ~ Country_Name + Market_Year + Commodity_Description, tradeData, sum)
setnames(temp, "oldLaggedConstantWeightedImportSubsidy", "oldLaggedFPSEconstant" )
tradeData <- merge(tradeData, temp, by = c("Country_Name", "Market_Year", "Commodity_Description"), all.x = TRUE)
summary(tradeData$oldLaggedFPSEconstant)
# lagged FPSE has similar descriptive statistics to unlagged
# deleting laggedOwnSubsidy since this already exists in PSE and is being merged in for the main script
tradeData[, LaggedOwnSubsidy := NULL]


# only imports from world are used in the wheat script, need to calculate exposure variable in this script
# so that it can be imported and used
# need to sum import subsidy for each year/country/commodity pairing
# can use aggregate function, will first create new column with weighted import subsidies and then sum them


temp <- aggregate(weightedImportSubsidy ~ Country_Name + Market_Year + Commodity_Description, tradeData, sum)
setnames(temp, "weightedImportSubsidy", "FPSE")

# checking to make sure the data isn't borked
quantile(temp$FPSE, na.rm = TRUE)
# nothing sums to 100 or above, data checks out
# aggregate works but the data has changed when I changed some names on some things, not sure this is working right
plot(temp$Market_Year, temp$FPSE)

# still, is the correct form and can be merged back in, will need to troubleshoot data though
tradeData <- merge(tradeData, temp, by = c("Country_Name", "Market_Year", "Commodity_Description"), all.x = TRUE)
summary(tradeData$FPSE)


# should probably go ahead and construct a weighted distance variable even if I am now skeptical of it
distance <- fread("distanceData.csv")
# this has countries tagged by 3 letter codes, can try to match with rt3ISO and pt3ISO in the trade dataset or try to find
# a different distance dataset- but definitely don't want to fill this out by hand since its ~ 200 x 200 datapoints
# 160 code differences, may not be usable, will need to check code for Dr. Sandoval project to see if I have already
# handled the naming issue- had not handled it, will need to fix it when I have internet, have ~80 countries not matched properly
# hopefully some of these are UN COMTRADE codes for trade pacts but I'm seeing a lot of eastern european and african countries
# that will not be merged properly

distance <- subset(distance, select = c(ida, idb, kmdist))
# renaming for easier matching
colnames(distance) <- c("rt3ISO", "pt3ISO", "distance(km)")

setdiff(unique(distance$rt3ISO), unique(tradeData$rt3ISO))
intersect(unique(distance$rt3ISO), unique(tradeData$rt3ISO))
# need to look at my distance data source and see what all the variables are, maybe the IDs in it can be used for a smoother
# merge; in any case I need internet to finish this part I think

tradeData <- merge(tradeData, distance, by = c("rt3ISO", "pt3ISO"), all.x = TRUE)

# need to converge to numeric for filtering purposes earlier in the script, converting back now because the other scripts
# depend on year being a dummy variable for regressions, which is automatically done by converting it to character
tradeData$Market_Year <- as.character(tradeData$Market_Year)

# need to save this data into an R file
save(tradeData, file = "tradeDataWeighted.RData")

# my trade data prep script is not firing for some reason, need to troubleshoot this, may be a problem with the new data?
# getting only a data conversion error but the cleaned data isn't appearing in changes to commit

# script does complete on my home computer, this is likely a syntax/package issue where something is loading
# differently on my office computer, issue may be using dplyr instead of dtplyr

mean(na.omit(tradeData$FPSEconstant - tradeData$oldFPSEconstant))
quantile(na.omit(tradeData$FPSEconstant - tradeData$oldFPSEconstant))
# average weighting change is quite small, or did I get the variables misaligned?

tradeData[,estimateDifference := FPSEconstant - oldFPSEconstant]
mean(na.omit(tradeData$estimateDifference))
quantile(tradeData$estimateDifference, na.rm = TRUE)
# results are the same, with the average difference being quite small




