unconstrained models are not labeled, and are the base case

all others are various runs of the Costinot et al model, both their baseline and my modified free trade with
distortions represented by the NRA removed

Country values are the sums of GDP, welfare, crop values after the model has completed

First order is first order welfare changes

Simple welfare appears to be an abandoned method

Trade constrained does not allow countries to move to exporting crops they did not previously

Allocation constrained can't rearrange field uses

IJK values are the individual trade flows(and autoconsumption)

1986 model is setting NRA back to pre WTO founding

