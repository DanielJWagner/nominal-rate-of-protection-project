# script to prepare quadratic psi effect counterfactuals

# clearing environment
rm(list = ls())

#install.packages("rmatio")
#install.packages("R.matlab")


# loading packages
library(data.table)
library(stargazer)
#library(rmatio)
library(R.matlab)

# loading in data
tauEpsData <- fread("ComtradeData/IJKValues_standardCostinotTauEpsInc.csv")

# making counterfactual matrices
# previous example of this is usaCounterfactualPrep.R


# We'll start with the base "no distortion" specification
quadraticLevelFieldMatrix <- tauEpsData[, list(`Producer Name`, `Importer Name`, `Crop ID`, `Crop Name`, `Importer ID`, `Producer ID`)]


# I  NRA for this
load("NRAclean.RData")
# need to shuffle the NRA in
# first as producer, then as importer, but I don't need the country code
NRA[, ccode := NULL]
names(NRA) <- c("Producer Name", "Year", "Crop Name", "Producer NRA")

# merging these in to have the NRA tracked before applying the effect

# we'll start with 2009 as the default
quadraticLevelFieldMatrix <- merge(quadraticLevelFieldMatrix, NRA[Year == "2009"], by = intersect(names(quadraticLevelFieldMatrix), names(NRA)),
                                 all.x = TRUE)
quadraticLevelFieldMatrix[,Year := NULL]

# now with importer NRA
names(NRA) <- c("Importer Name", "Year", "Crop Name", "Importer NRA")
quadraticLevelFieldMatrix <- merge(quadraticLevelFieldMatrix, NRA[Year == "2009"], by = intersect(names(quadraticLevelFieldMatrix), names(NRA)),
                                 all.x = TRUE)
quadraticLevelFieldMatrix[,Year := NULL]

# I need to calculate psi here for reference, filling in NAs with 0s so I can 
# get a valid result
quadraticLevelFieldMatrix[is.na(`Producer NRA`)]$`Producer NRA` <- 0
quadraticLevelFieldMatrix[is.na(`Importer NRA`)]$`Importer NRA` <- 0

# calculating psi, change in implementation for quadrtic is the next step
quadraticLevelFieldMatrix[, psi := (1 + `Importer NRA`)/(1 + `Producer NRA`)]
# quadraticLevelFieldMatrix[`Importer Name` == `Producer Name`]
# don't need to set NRA to 1 because it already is


# loading in the effects for the quadratic term
# from residualNetNRATest2 in the residualRegression.R script
psiEffect <- -2.19945
psiSquaredeffect <- 0.30678


# original distortion matrix
# distortionMatrix$NetImporterNRA <- (distortionMatrix$NetImporterNRA-1)*ePercentIncreaseNewDeltaTau*-1 +1


# so I'm just setting psi equal to 1 and looking at the quadratic change
# but that isn't linear?
# applying the actual quadratic effect
quadraticLevelFieldMatrix[, quadraticEffect := 1 +  ((exp(psiEffect*(psi -1) + psiSquaredeffect*(psi-1)^2 )-1))]
# I need to bifurcate this by positive and negative?
# or no just adjust it, this seems right? or the bounds make sense

summary(quadraticLevelFieldMatrix)



# pre-WTO specification, if I have time








# writing these to matlab matrices
quadraticLevelPlayingField <- array(quadraticLevelFieldMatrix$quadraticEffect, c(50,50,10))


# actually writing after breaking them out
writeMat("quadraticLevelPlayingField.mat", quadraticLevelPlayingField = quadraticLevelPlayingField)
