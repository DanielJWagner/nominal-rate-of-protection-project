clear all
clc

% Run all phases for all models. For ease of computation time, the
% equilibrium prices are precoded as the initial starting points. The code
% can be re-run with other initial points, but convergence is slow and
% sometimes fails (numerically).

% Add in code folders
addpath(genpath('Helpers'));
addpath(genpath('Code (JPE)'));

% Home folder
% rewriting to match my office computer
homePath = 'C:\Users\wagnerdnlj\Documents\nominal-rate-of-protection-project\CostinotEtAl2016supplementalFiles';

% Model names
% can probably leave prefixes?
% these cycle through difference scenarios for each model when calculating
% welfare
modelPrefixes = {'bl','cc','tc','ac'};
 %allModels = {'standard', 'freeTrade'};
 %allModels = {'1986'};
 %allModels = {'usWheatIncrease', 'usIncrease', 'worldLessUSIncrease'};
 %allModels = {'standardDouble'};
 %allModels = {'freeTrade'};
 %allModels = {'linearFreeTrade'};
%allModels = { 'ChinaBarrierRemoval', 'JapanBarrierRemoval', 'SKbarrierRemoval', 'usIsolation'};
allModels = {'quadratic'};
%allModels = {'standard','multipletheta','projectedTaueps','mixedGAEZ','intranational','GAEZAlt_bRF_H_MAIN_fC2A2_RF_H_ALT','GAEZAlt_bRF_H_MAIN_fC2B2_RF_H_ALT','GAEZAlt_bRF_H_MAIN_fCSA2_RF_H_ALT','GAEZAlt_bRF_H_MAIN_fCSB1_RF_H_ALT','GAEZAlt_bRF_H_MAIN_fCSB2_RF_H_ALT','GAEZAlt_bRF_H_MAIN_fEHA2_RF_H_ALT','GAEZAlt_bRF_H_MAIN_fEHB2_RF_H_ALT','GAEZAlt_bRF_H_MAIN_fH3A2_RF_H_ALT','GAEZAlt_bRF_H_MAIN_fH3B1_RF_H_ALT','GAEZAlt_bRF_H_MAIN_fH3B2_RF_H_ALT'};
 ccOnlyModels = logical([0]); % worried about what removing this script
% will do
% ccOnlyModels = logical([0 0 0 0 0 1 1 1 1 1 1 1 1 1 1]);
 sensitivityModels = logical([0]); 
 % sensitivity only runs checks, does not save/run earlier portion of results
% sensitivityModels = logical([0 1 1 0 1 0 0 0 0 0 0 0 0 0 0]);

% Precode solutions
% there's apparently a lot in this script
% I need to remove this part but I need to find out if later scripts are
% dependent on it or it just sort of folds in
% the data in precoded solutions is called on later as well
% these appear to just be starting points
 PrecodedSolutions

% Run through the general models
% can add/remove models here, appears to scale based on the number of names
for modelIX = 1:size(allModels,2)
    % Clear all variables except for our list of models & precoded
    % solutions
    clearvars -except generalModels ccOnlyModels homePath allModels modelPrefixes modelIX sensitivityModels
    
    % Model name
    model_name = allModels{modelIX};
    
    % Selection parameters
    N = 50; % # of countries, 100 is too many or the land data has to be
    % changed to make this work?
    % 75 works
    K = 10; % # of crops
    
    % GAEZ data sources
    % loading data for different assumptions
    if strcmp(model_name,'mixedGAEZ')
        gaezBSource = 'GAEZ_mixedHL_b';
        gaezFSource = 'GAEZ_mixedHL_fH3A1';
    elseif ~ccOnlyModels(modelIX)
        gaezBSource = 'GAEZ_scenario_b_RF_H_MAIN';
        gaezFSource = 'GAEZ_scenario_fH3A1_RF_H_MAIN';
    else
        gaezBSource = 'GAEZ_scenario_b_RF_H_MAIN';
        % pulling in different scenarios based on model number
        % for the climate data
        gaezFSource = ['GAEZ_scenario_' model_name(20:end)];
    end
    
    % Setup only once for each set of GAEZ data
    if ~sensitivityModels(modelIX)
        % Run the setup (model name & sources from above)
        disp(['Setting up model ' model_name]);
        Master_Setup
    end
end

for modelIX = 1:size(allModels,2)
    % Model name
    model_name = allModels{modelIX};
    disp(['Calibration/equilibrium for model ' model_name]);
    
    % Calibration (standard model only)
    % if strcmp(model_name,'standard')
    % I need to calibrate the free trade model at least once apparently
    % as it depends on the processed file
        % Run the calibration
        clearvars -except model_name generalModels ccOnlyModels homePath allModels modelPrefixes modelIX sensitivityModels
        Master_Calibrate
    % end    
    
    % Run the equilibrium computations
    clearvars -except model_name generalModels ccOnlyModels homePath allModels modelPrefixes modelIX sensitivityModels
    load('ModelNamesSolutions.mat');
    for prefixIX = 1:size(modelPrefixes,2)
        mPrefix = modelPrefixes{prefixIX};
        
        % Initial prices (precoded as solutions)
        if ~ccOnlyModels(modelIX) || ~strcmp(mPrefix,'bl')
            % In most cases, the prices are precoded
            eval(['p0_' mPrefix ' = p0_' mPrefix '_' model_name ';']);
        else
            % For new CC scenarios in the baseline case, the price
            % solutions are the same as those in the standard model
            eval(['p0_' mPrefix ' = p0_' mPrefix '_standard;']);
        end
    end
    Master_Equilibrium
    
    % Sensitivity--standard model only; used to generate other scenarios
    if strcmp(model_name,'standard')
        clearvars -except model_name generalModels ccOnlyModels homePath allModels modelPrefixes modelIX sensitivityModels
        Master_Sensitivity
    end
end

% For comparisons in the figures, compute some quantities at given FAO prices
% to wrap up
FAOPriceOutput