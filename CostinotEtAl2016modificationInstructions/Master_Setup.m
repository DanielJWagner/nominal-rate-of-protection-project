% Runs all the files which set up the model

% Conventions:
% i is an exporter, j is an importer, k is a crop
% Wide-format variables are stored v_ijk in three dimensions (i, j, and k
% respectively).
% Two-index variables are stored inconsistently--this is because
% they will be used in different ways and they are stored for coding
% simplicity. They may be stored as a 2-D matrix (m X n) or 3-D (m X 1 X n)
% Some input lists are stored as a 1-D matrix (m*n X 1)
% GAEZ variable (A) is stored as 2-D field X crop

% Run Setup code
LoadChooseCoverage
ComputeSigma
ComputeKappa
ComputeZeta

% Run consistency tests (in particularly on demand function parameters)
% Prep:
p0_ijk = repmat(p0_ik,[1 N 1]);

% Compute expected demand
xx1 = p0_ijk .* Demandijk(p0_ik,sigma_ijk,kappa,taueps_ijk,1,zeta,az_j);
xx2 = p0_ijk .* Demandijk(p0_ik,sigma_ijk,kappa,taueps_norm,beta_norm,zeta,az_norm);

disp('Consistency (random prices)');
p_test = 100*rand(N,1,K);
c1 = Demandijk(p_test,sigma_ijk,kappa,taueps_ijk,1,zeta,az_j);
c2 = Demandijk(p_test,sigma_ijk,kappa,taueps_norm,beta_norm,zeta,az_norm);
disp(['Max relative consistency error ' num2str(max(abs(1 - (c1(:)./c2(:)))))]);

% Errors
e1 = max( abs(xx1(:) - X_ijk(:)) );
e2 = max( abs(xx2(:) - X_ijk(:)) );

disp(['Error from test 1 is: ' num2str(e1)]);
disp(['Error from test 2 is: ' num2str(e2)]);

% Clean up
clear xx1 xx2 e1 e2 X_j agY P_j P_j2 P_jk p0_ijk c1 c2 p_test

% Save final output
disp('Saving final model...');
save(['In/Processed/Model_' model_name '0.mat']);