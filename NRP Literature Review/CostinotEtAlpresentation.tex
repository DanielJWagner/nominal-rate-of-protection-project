\documentclass{beamer}
\usepackage{graphicx}
\graphicspath{ {images/} }

\usetheme{default}

\setbeamertemplate{footline}[frame number]

\title{Evolving Comparative Advantage and the Impact of Climate Change in Agricultural Markets: Evidence from 1.7 Million Fields around the World}
\author{Arnaud Costinot, Dave Donaldson, Cory Smith}
\institute{presentation by Daniel Wagner}

\begin{document}
	
	\frame{\titlepage}
	
	\begin{frame}
	\frametitle{Agenda}
	\begin{itemize}
		\item Introduction
		\item Data
		\item Model
		\item Estimation
		\item Counterfactual Results
		\item Contribution \& Summary
	\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Introduction}
\begin{itemize}
	\item CDS(Cosninot, Donaldson, Smith) study adjustment to climate change shocks to productivity
	\begin{itemize}
		\item Adjustment via trade, comparative advantage dampens shock
		\item Use productivity microdata to look at production adjustment
	\end{itemize}
	\item Use framework similar to Eaton Kortum (2002)
	\begin{itemize}
		\item Estimate parameters and run counterfactual for projected climate change
		\item Base model and with fixed trade and production allocations 
		
	\end{itemize}
\item Contribution- combine subnational production data to provide world welfare simulations under climate change
\end{itemize}

\end{frame}



\begin{frame}{Data: Global Agro-Ecological Zones Project}
\begin{itemize}
		\item GAEZ data on agricultural productivity of 1.7 million fields
		\item 5-arc-minute climate data
	\begin{itemize}
		\item Roughly 1,000 to 8,000 hectares depending on lattitude
	\end{itemize}
		\item Based on 1960-1990 weather average, land characteristics
		\item Includes estimates for field/crop combinations not in use
		\item Also includes productivity under estimated climate change
		\begin{itemize}
			\item Estimates from 2070-2100, project various assumptions
		\end{itemize}
	\item Maximum of wet and dry rice production used
\end{itemize}
\end{frame}

\begin{frame}
\includegraphics[scale=.5]{map}
\end{frame}

\begin{frame}
\frametitle{Data}

\begin{itemize}
	\item 50 countries and 10 crops in 2009
	\begin{itemize}
		\item Majority of World Crop Value 
		\item Data on agricultural output, land use, and prices from FAOSTAT
		\item GAEZ fields mapped to countries with World Borders Dataset
		\item Trade flows from UN Comtrade
	\end{itemize}
	
\end{itemize}

\end{frame}

\begin{frame}{Data Summary}
\includegraphics[scale=.45]{table1}
\end{frame}

\begin{frame}{Simple Two-Country Model}

\begin{itemize}
	\item Two Islands, Two Fields Each, Two Crops: Rice and Wheat
		\begin{itemize}
			\item North and South Island, East and West fields in each
		\end{itemize}
	\item Consumers spend 50\% of income on each crop
	\item Climate Change reduces productivity heterogenously
	\begin{itemize}
		\item South Island: No effect on rice, wheat production falls to 0 in Western field and is halved in East
		\item North Island: No effect on wheat, rice production falls to 0 in Eastern field and half of previous production in West 
	\end{itemize}
\end{itemize}

\end{frame}

\begin{frame}{Simple Two-Country Model}

\begin{itemize}
	\item North specializes in wheat, South specializes in rice
	\item Total Welfare is unaffected
	\begin{itemize}
	\item Even dropping no trade costs assumption, little effect for -50 to -100\% productivity shock
	\end{itemize}
\item Still some ability to compensate in autarky
\begin{itemize}
	\item Move production of affected crop to less affected field
\end{itemize}
\item CDS simulate these adjustments on a larger scale
\end{itemize}

\end{frame}



\begin{frame}{Theoretical Model}

\begin{itemize}


\item \[U_i = C^0_i + \beta_i ln C_i\]
\item Income effects are eliminited by quasi-linear utility.

\item \[C_i = \left[\sum_{k \in K}(\beta_i^k)^{1/\sigma_k}(C^k_i)^{(\sigma_k-1)/\sigma_k} \right]^{\sigma_k/(\sigma_k-1)} \]

\item $C_i$ being aggregate consumption of crops $k \in K$ by individual $i$
\item $\sigma_k$ = elasticity of substitution between crops, 
\item $\sigma$ between varieties, $C^k_{ij}$ consumption of variety from country $j$

\item \[C_i^k= \left[\sum_{j\in I} (\beta^k_{ij})^{1/\sigma}(C^k_{ij})^{(\sigma -1)/\sigma} \right]^{\sigma /(\sigma - 1)} \]
\item Crops relatively homogeneous but assume CES for tractability

\end{itemize}

\end{frame}

\begin{frame}{Production}

\begin{itemize}
	
	\item As in Eaton and Kortum (2002) Total Factor Productivity and labor intensity are independently drawn from a Fr\`echet
	\item \[Pr\{A^{f1}_i(\omega)\leq a^1,...,A^{fK}_i\leq a^K, v_i^f(\omega)\leq v \} \]
	\[= exp\left\{-\gamma\left[\sum_{k\in K}(a^k/A^{fk}_i)^{-\theta}+(v/v_i)^{-\theta} \right] \right\} \]
	\item where $\theta$ is technological heterogeneity
\end{itemize}

\end{frame}

\begin{frame}{Production}
\begin{itemize}
	\item we fix wage $w_i$ at the productivity of the outside good
	\item $w_i = A^0_i$, which is a random variable
	\item Endogenous crop selection $\pi_i^{fk}\equiv Pr\{p_i^kA^{fk}_i(\omega)\} $
	\item Since as in EK the probability of crops in a field equals share
	\item \[\pi^{fk}_i\equiv \frac{(p^k_iA^{fk}_i(\omega))^\theta}{\alpha^\theta_i +\sum_{l\in K}(p^l_iA^{fl}_i)^\theta},  \alpha_i\equiv A_i^0v_i \]
	\item Space allocated to a crop is proportional to price, efficiency
	
\end{itemize}

\end{frame}

\begin{frame}{Competitive Equilibrium}
\begin{itemize}
	\item Market clearing condition:
	\item \[Q_i^k = \sum_{j\in \mathcal{I}}\tau^k_{ij} C^k_{ij} \]
	\item Equilibria are simulated under base GAEZ
	\item Also simulated for a variety of climate change scenarios
\end{itemize}
\end{frame}

\begin{frame}{Assumptions}
\begin{itemize}
	\item Crops are differentiated by country
	\item "High inputs", no irrigation assumed for estimated productivity
	\begin{itemize}
		\item Not the case for some countries
	\end{itemize}
	\item Also assumed all estimated land is available for cultivation
	\item Missing prices are estimated with fitted values
\end{itemize}
\end{frame}

\begin{frame}{Estimating Parameters: 1st level, Crop Variety Demand}
\begin{itemize}
\item 3 tiered estimation structure for 3 levels of demand
\item \[ln(X^k_{ij}/X_j^k) = M_j^k +(1-\sigma)ln(p_i^k) +\varepsilon^k_{ij} \]
\item with price IV $Z^k_i \equiv \left(\frac{1}{F_i}\sum_{f\in \mathcal{F}_i}A^{fk}_i  \right)$
\item fixed effect $M_j^k \equiv -ln\left[\sum_{n \in \mathcal{I}:X^k_{nj}>0 } \beta^k_{nj}(\tau^k_{nj}p^k_n)^{1-\theta} \right] $
\item $\varepsilon_{ij}^k \equiv ln\left[\beta^k_{ij}(\tau^k_{ij})^{1-\theta} \right] $ solved for from residuals
\item assume $\sum_{i \in \mathcal{I}:X^k_{ij}>0 }\varepsilon_{ij}^k = 0$ without loss of generality
\item  cannot separate demand shifter $\beta^k_{ij}$ and trade costs $\tau_{ij}^k$ 
\item  separation not needed for simulation

\end{itemize}
\end{frame}

\begin{frame}{Estimating Parameters: 2nd level, Crop Demand }
\begin{itemize}
	\item Crop level estimation is similar
	\item \[ln\left(X_j^k/X_j \right) = M_j + (1-\sigma_k)ln(P^k_j) + \varepsilon^k_j \]
	\item Importer fixed effect $M_j\equiv -ln\left[\sum \beta^l_j(P^l_j)^{1-\sigma_k} \right] $
	\item construct $P_j^k\equiv \left[\sum_{i\in \mathcal{I}}\beta_{ij}^k(\tau^k_{ij}p^k_i)^{1-\sigma} \right]^{1/(1-\sigma)}$ using estimates from previous step
	\item $\varepsilon^k_j\equiv ln(\beta^k_j)$ and normalized to 0, estimated from residuals
	\item Step 3: $\beta_j = X_j$ can be read directly from trade share data
\end{itemize}
\end{frame}

\begin{frame}{Estimating Parameters: Supply}
\begin{itemize}
	\item Productivity is observed in GAEZ leaving us with
	\item \[Q_i^k(\theta,\alpha_i)\equiv \sum_{f\in \mathcal{F}_i} s_i^fA_i^{fk}\left[\frac{(p_i^kA_i^{fk})^{\theta}}{\alpha_i+\sum(p^l_iA^{fl}_i)} \right]^{(\theta-1)/\theta} \]
	\item with land allocation
	\item \[L_i(\theta,\alpha_i)\equiv\sum_{k\in \mathcal{K}} \sum_{f\in \mathcal{F}_i}s^f_i\left[\frac{(p^{\theta}_iA^{fk}_i)^\theta }{\alpha^{\theta}_i+\sum_{i\in \mathcal{K}}(p^l_iA^{fl}_i)^\theta } \right] \]
	\item due to lack of field production data forced to use NLS for $Q^k_i$
    \item subject to $L_i(\theta,\alpha_i)=L_i$ for all $i\in \mathcal{I}$
	
\end{itemize}
\end{frame}

\begin{frame}{Estimated Parameters}
\includegraphics[scale=.5]{table2}\footnote{$\sigma_k$ is denoted k in the paper, I have changed it for ease of reading}
\end{frame}

\begin{frame}{Goodness of Fit}
\includegraphics[scale=.5]{outputFigure}
\end{frame}

\begin{frame}{Goodness of Fit}
\begin{itemize}
	\item CDS claim $\sigma$ is similar to Broda \& Weinstein
	\item Crop varietes are highly elastic, as would be expected with little differentiation
	\item Trade share, revenue share, land use also fit well
	\begin{itemize}
		\item $\beta$ covers unobserved shocks, trade fits observed prices exactly
	\end{itemize}
	\item $\theta$ is similar to micro results from Peruvian data
\end{itemize}
\end{frame}

\begin{frame}{Counterfactual Results}
\includegraphics[scale=.5]{table4}
\end{frame}

\begin{frame}{Counterfactual Results}
\begin{itemize}
	\item Test different climate change estimates as productivity shock
	\item Welfare losses small but unevenly distributed
	\begin{itemize}
		\item Malawai loses 49.07\% GDP under full adjustment, ~ 80\% with no production adjustment
	\end{itemize}
\item Adjustment mutes welfare losses significantly
\item No trade adjustment fixes exports as \% of production
\end{itemize}
\end{frame}

\begin{frame}{Counterfactuals: Sensitivity Analysis}
\begin{itemize}
\item CDS run counterfactuals under variety of scenarios
\begin{itemize}
	\item Also fix production and trade patterns
\end{itemize}
\item Treat 0 trade flows as "missing" to attempt to model extensive margin
\begin{itemize}
	\item Trade effect expands but still relatively small
\end{itemize}
\item Allowing heterogeneous inputs, $\theta$, has little effect
\begin{itemize}
	\item Developing world similarly worse off before and after climate change
\end{itemize}
\item Intranational trade has little impact, 
\begin{itemize}
\item Productivity shocks not correlated with distance from capital
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Counterfactual: Alternative Model Assumptions}
\includegraphics[scale=.5]{table5}
\end{frame}

\begin{frame}{Summary}

\begin{itemize}
	\item Trade adjustment fairly unimportant in compensating climate change
	\begin{itemize}
		\item But only considered "agriculturally significant" countries
		\item Import-dependent nations may be more affected
	\end{itemize}
	\item Contribution 
	\begin{itemize}
		\item Allow within country production variance with GAEZ dataset
		\item More complete study of production and trade adjustment than previously possible
		\item Integrate granular growing predictions into world level welfare prediction
	\end{itemize}
\end{itemize}


\end{frame}

\begin{frame}{References}

\begin{itemize}
	

\item
Jonathan Eaton, Samuel Kortum. 2002. "Technology, Geography, and Trade." Econometrica no. 5: 1741. JSTOR Journals, EBSCOhost (accessed April 5, 2018).

\item
Costinot, Arnaud, Dave Donaldson, and Cory Smith. "Evolving Comparative Advantage and the Impact of Climate Change in Agricultural Markets: Evidence from 1.7 Million Fields around the World." Journal Of Political Economy 124, no. 1 (February 2016): 205-248. Business Source Premier, EBSCOhost (accessed April 10, 2018).

\end{itemize}

\end{frame}

\end{document}
