# script to prepare supply and possibly demand elasticities to calculate
# subsidy leakage

# clearing environment
rm(list = ls())

# loading packages
library(data.table) # standard packages for data/presentation
library(stargazer)
# library(R.matlab) # might need this later?


# loading in price data for the standard model
standardDetailed <- fread("SimulationResults/IJKvalues_standard_bl.csv")
summary(standardDetailed)
# based on how the model is written producer price is probably not the consumer price index
# but the little p used in decision making
# this is little p, price indices are calculated by the model for individual crops but not reported except
# for the final price index of all crops
# P_jk = Pjk(taueps_norm, reshape(p_EQBase,[N 1 K]), sigma_ijk);
# P_j = Pj(beta_norm,P_jk,kappa);
# Pj and Pjk are custom methods, probably the equation used to index

# 'raw' prices are not real world prices but are prices without 0s and infinites
# so some very small & large values have been used to replace them

# loading in land use data in the standard model
standardMostDetailed <- fread("SimulationResults/FieldValues_standard_bl.csv")
summary(standardMostDetailed)

# loading in standard for comparison
standardBase <- fread("SimulationResults/CountryValues_standard_bl.csv")
summary(standardBase)
# crop price index is here, but it's for crops overall?
# but it specifies, so for 

# loading in raw land productivity data for the base model
# which should be 'current' 30 year average
landData <- fread("GAEZdata/GAEZ_scenario_b_RF_H_MAIN.csv")
summary(landData)
# I'm not sure this is the same ID system?  but Costinot et al make significant
# cuts by deleting fields with 0 in all yields
# how do I check 38 observations at once? probably just write it out

landData <- landData[!(yield1 == 0 & yield2 == 0 & yield3 == 0 & yield4 == 0 & yield5 == 0 & yield6 == 0 & yield7 == 0 & 
             yield8 == 0 & yield13 == 0 & yield18 == 0 & yield23 == 0 & yield28 == 0 & yield33 == 0 & yield38 == 0 & 
             yield9 == 0 & yield14 == 0 & yield19 == 0 & yield24 == 0 & yield29 == 0 & yield34 == 0 &  
             yield10 == 0 & yield15 == 0 & yield20 == 0 & yield25 == 0 & yield30 == 0 & yield35 == 0 &  
             yield11 == 0 & yield16 == 0 & yield21 == 0 & yield26 == 0 & yield31 == 0 & yield36 == 0 &  
             yield12 == 0 & yield17 == 0 & yield22 == 0 & yield27 == 0 & yield32 == 0 & yield37 == 0  )]

# ID distributions match after cutting land that is never productive
# I need to standardize some names here for matching
names(standardMostDetailed) # I will default to these
names(landData) <- c("Country name", "Field ID",      "cid"     ,     "Field Area",    "yield1" ,      "yield2" ,       "yield3",      
                      "yield4"      , "yield5"  ,     "yield6"  ,     "yield7"    ,   "yield8"  ,     "yield9"  ,     "yield10",     
                      "yield11"      ,"yield12"  ,    "yield13" ,     "yield14"   ,   "yield15" ,     "yield16" ,     "yield17",     
                      "yield18"      ,"yield19" ,     "yield20" ,     "yield21"   ,   "yield22" ,     "yield23" ,     "yield24",     
                      "yield25"      ,"yield26" ,     "yield27" ,     "yield28"   ,   "yield29" ,     "yield30" ,     "yield31",     
                      "yield32"      ,"yield33" ,     "yield34" ,     "yield35"   ,   "yield36" ,     "yield37" ,     "yield38")
intersect(names(standardMostDetailed), names(landData))
standardMostDetailed <- merge(standardMostDetailed, landData) # I lose only about ~30 obs with this merge without country IDs
# should be good enough

# the smaller detailed dataset has to merge into this
summary(standardDetailed)
# looking at this from producer perspective for supply elasticity
# so I need to set producer to country name, but also change crop name

intersect( names(standardMostDetailed), names(standardDetailed)) # need to reconcile some names
# I will change the most detailed name to producer to clarify this is from that perspective?
names(standardMostDetailed)[names(standardMostDetailed) == "Country name"] <- "Producer Name"
names(standardMostDetailed)[names(standardMostDetailed) == "Country ID"] <- "Producer ID"

# this also needs to be by crop? I might need to break most detailed down by crop and fold it in
unique(standardDetailed$`Crop ID`)
# merge(standardMostDetailed, standardDetailed, by = intersect( names(standardMostDetailed), names(standardDetailed)))

# split standardMostDetailed out by crop, and add a crop ID
# cropOneData <- standardMostDetailed[, list(`Producer Name`, `Field ID`, `Field Area`, `Producer ID`,
#                                           `Land use (portion) for crop 1`, `yield1`)]
# cropOneData$`Crop ID` <- 1
# or should I do this by breaking out standard detailed and matching it to yields?
# what I ultimately need with each observation is theta, which I can add
# the size * productivity of all relevant fields in a specific crop
# and the productivity of all other crops, so this needs to be assembled in the most detailed
# framework so that I have yields for every crop

# so I need to be breaking price out by crop/country and folding it into most detailed, still breaking things out
# and sticking back in but parting out the price data, not the land data

cropOneData <- standardDetailed[`Crop ID` == 1][, list(`Producer Name`, `Crop Name`, `Producer ID`, `Crop ID`, `Producer price (equilibrium)`, `Lambda-Wage (producer)`)] 
# I need to do this by ID so it's easier to call things
# probably cut wage on later iterations?
# need to trim this data as I'll be shuffling this in 10 times, rename any shared variables that aren't just for matching country + field
# just do yield1, price1, etc
names(cropOneData) <- c("Producer Name", "Crop One Name", "Producer ID", "Crop One ID", "Crop One Price", "Wage")
summary(cropOneData) # I can cut all the raw variables, 

cropOneData <- unique(cropOneData)

standardMostDetailed <- merge(standardMostDetailed, cropOneData, by = intersect(names(standardMostDetailed), names(cropOneData)))
# this is multiplying in size, why?  should just join to every field, fields are repeated but it shouldn't be multiplying?
# oh, standard most detailed is for trade flows, but I've dropped that data and prices are the same so now the merge works

# breaking the rest of the data out, filtering out duplicates
cropTwoData <- unique(standardDetailed[`Crop ID` == 2][, list(`Producer Name`, `Crop Name`, `Producer ID`, `Crop ID`, `Producer price (equilibrium)`)])
cropThreeData <- unique(standardDetailed[`Crop ID` == 3][, list(`Producer Name`, `Crop Name`, `Producer ID`, `Crop ID`, `Producer price (equilibrium)`)])
cropTenData <- unique(standardDetailed[`Crop ID` == 10][, list(`Producer Name`, `Crop Name`, `Producer ID`, `Crop ID`, `Producer price (equilibrium)`)])
cropFourteenData <- unique(standardDetailed[`Crop ID` == 14][, list(`Producer Name`, `Crop Name`, `Producer ID`, `Crop ID`, `Producer price (equilibrium)`)])
cropTwentyData <- unique(standardDetailed[`Crop ID` == 20][, list(`Producer Name`, `Crop Name`, `Producer ID`, `Crop ID`, `Producer price (equilibrium)`)])
cropTwentyFourData <- unique(standardDetailed[`Crop ID` == 24][, list(`Producer Name`, `Crop Name`, `Producer ID`, `Crop ID`, `Producer price (equilibrium)`)])
cropTwentyEightData <- unique(standardDetailed[`Crop ID` == 28][, list(`Producer Name`, `Crop Name`, `Producer ID`, `Crop ID`, `Producer price (equilibrium)`)])
cropTwentyNineData <- unique(standardDetailed[`Crop ID` == 29][, list(`Producer Name`, `Crop Name`, `Producer ID`, `Crop ID`, `Producer price (equilibrium)`)])
cropThirtyThreeData <- unique(standardDetailed[`Crop ID` == 33][, list(`Producer Name`, `Crop Name`, `Producer ID`, `Crop ID`, `Producer price (equilibrium)`)])

# setting up names so this can all go one one observation
names(cropTwoData) <- c("Producer Name", "Crop Two Name", "Producer ID", "Crop Two ID", "Crop Two Price")
names(cropThreeData) <- c("Producer Name", "Crop Three Name", "Producer ID", "Crop Three ID", "Crop Three Price")
names(cropTenData) <- c("Producer Name", "Crop Ten Name", "Producer ID", "Crop Ten ID", "Crop Ten Price")
names(cropFourteenData) <- c("Producer Name", "Crop Fourteen Name", "Producer ID", "Crop Fourteen ID", "Crop Fourteen Price")
names(cropTwentyData) <- c("Producer Name", "Crop Twenty Name", "Producer ID", "Crop Twenty ID", "Crop Twenty Price")
names(cropTwentyFourData) <- c("Producer Name", "Crop Twentyfour Name", "Producer ID", "Crop Twentyfour ID", "Crop Twentyfour Price")
names(cropTwentyEightData) <- c("Producer Name", "Crop Twentyteight Name", "Producer ID", "Crop Twentyteight ID", "Crop Twentyteight Price")
names(cropTwentyNineData) <- c("Producer Name", "Crop Twentynine Name", "Producer ID", "Crop Twentynine ID", "Crop Twentynine Price")
names(cropThirtyThreeData) <- c("Producer Name", "Crop Thirtythree Name", "Producer ID", "Crop Thirtythree ID", "Crop Thirtythree Price")

# folding back together
stanardMostDetailed <- merge(standardMostDetailed, cropTwoData, by = c("Producer Name", "Producer ID"))
stanardMostDetailed <-merge(standardMostDetailed, cropThreeData, by = c("Producer Name", "Producer ID"))
stanardMostDetailed <-merge(standardMostDetailed, cropTenData, by = c("Producer Name", "Producer ID"))
stanardMostDetailed <-merge(standardMostDetailed, cropFourteenData, by = c("Producer Name", "Producer ID"))
stanardMostDetailed <-merge(standardMostDetailed, cropTwentyData, by = c("Producer Name", "Producer ID"))
stanardMostDetailed <-merge(standardMostDetailed, cropTwentyFourData, by = c("Producer Name", "Producer ID"))
stanardMostDetailed <-merge(standardMostDetailed, cropTwentyEightData, by = c("Producer Name", "Producer ID"))
stanardMostDetailed <-merge(standardMostDetailed, cropTwentyNineData, by = c("Producer Name", "Producer ID"))
stanardMostDetailed <-merge(standardMostDetailed, cropThirtyThreeData, by = c("Producer Name", "Producer ID"))

# now I just need theta and alpha and to actually write the calculation
# theta is 2.46 with 95% confidence interval of [2.28, 2.62]
theta <- 2.46
# sum over all fields field size * productivity of said field for a specific crop * (theta - 1) * 
standardMostDetailed[, sum(`Field Area`*yield1*(theta-1))]
names(stanardMostDetailed)

# I should probably have looked at if demand elasticity is possible before digging into this
# looking at consumption of individual crops C^k_i, but price does not enter directly
# unclear if the price I got in supply is the lower case p, I think it is? but need to check
# matlab code

# I need to reconstitute the price indices here
# for demand elasticity I need the demand fixed effect for the consuming country,
# demand fixed effect for the country/crop paring, and the demand shifter tau eps term
# for every trading partner for a given crop- and I need all of this for all crops
# more labor intensive to solve for, but I should have some of this?
# betas are not reported,  calibrated earlier like tauEps
# doable but more labor intensive unless I can save them in Matlab?
# yes, these are calculated in matlab and I can extract them there
# though some need to be manually assembled for the derivative to account for
# 2nd order effects

