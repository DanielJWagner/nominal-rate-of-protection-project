# script to clean up population to make some per capita calculations at other points
# just breaking down the world bank data into a better format and saving as RData

# clearing environment
rm(list = ls())

# loading packages
library(data.table)

# need this to match with my NRA data
load("NRAclean.RData")

# loading in the population data and slicing off the first few rows
populationData <- fread("ComtradeData/worldBankCountryPopulation.csv", skip = 4, header = TRUE)
summary(populationData)
# have the variables named correctly at least

# I just need the 2009 data for my purposes, saves me the trouble of melting & recasting this
pop2009 <- populationData[, c("Country Name", "2009")]
summary(pop2009)


# just need to match some names and repeat the corrections I've already made
names(NRA)
names(pop2009) <- c("country", "2009 population")


intersect(NRA$country, pop2009$country)
# only 74 matches initially, might be a problem

# repeating NRA name changes
pop2009[country == "Burkinafaso"]$country <- "Burkina Faso"
pop2009[country == "Coted'ivoire"]$country <- "Cote d'Ivoire"
pop2009[country == "Czechrep"]$country <- "Czech Republic"
pop2009[country == "Dominicanrepublic"]$country <- "Dominican Republic"
pop2009[country == "Korea"]$country <- "Korea Republic of"
pop2009[country == "Newzealand"]$country <- "New Zealand"
pop2009[country == "Rsa"]$country <- "South Africa"
pop2009[country == "Srilanka"]$country <- "Sri Lanka"

pop2009[country == "Tanzania"]$country <- "United Republic of Tanzania"
pop2009[country == "Uk"]$country <- "United Kingdom"
pop2009[country == "Us"]$country <- "United States"
pop2009[country == "Vietnam"]$country <- "Viet Nam"

# I can probably just afford to lumb Belgium and Luxembourg together?
pop2009[country == "Bel-Lux"]$country <- "Belgium"
# since NRA isn't volume dependent

setdiff(NRA$country, pop2009$country) # but only missing 4 countries now from
# the NRA set
setdiff( pop2009$country, NRA$country)
#pop2009[country == "Iran, Islamic Rep."]$country <- "Iran" # need this to not be fixed for standard base
pop2009[country == "Russian Federation"]$country <- "Russia"
pop2009[country %like% "Egypt"]$country <- "Egypt"

NRA[country %like% "Korea"]
pop2009[country %like% "Korea, Rep."]$country <- "Korea Republic of"

# missing these countries from the standard base file
#[1] "Burma"                            "Democratic Republic of the Congo" "Iran (Islamic Republic of)"      
#[4] "Venezuela"           

pop2009[country == "Iran, Islamic Rep."]$country <- "Iran (Islamic Republic of)"
pop2009[country %like% "Myanmar"]$country <- "Burma"
pop2009[country %like% "Venezuela"]$country <- "Venezuela"
pop2009[country %like% "Congo, Dem."]$country <- "Democratic Republic of the Congo"

# saving the data
save(pop2009, file = "pop2009.RData")





