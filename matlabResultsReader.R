# script to read in matlab results which takes a long time
# don't want to clog up the welfare results script and not sure if anything crucial is here

# clearing environment
rm(list = ls())

# loading packages
library(data.table)
library(stargazer)
library(R.matlab)

# make sure to have these results in the folder, they're too large to put on the repo
standardMatlabResults <- readMat("SimulationResults/Model_standard_blSolutions.mat")



summary(standardMatlabResults)
# I can probably pull these individually?
standardMatlabResults$W.j
# yes, pulling variables individually works, but none of these are the outside good
standardMatlabResults$C.j
# C.j is just the utility of the final consumption bundle?
standardMatlabResults$Q.ijk
# I can maybe break this out? but there are only 10 good slices, outside good is only implied?
standardMatlabResults$landUsed
# land used can be used to ferret out the outside good
# but that's also in the country values, ultimately not sure how necessary this script is