# script to calculate gross subsidy expenditure/equivalent
# the amount spent on subsidy measures
# hopefully also piecing out the amount spend on subsidizing foreign consumption

# clearing environment
rm(list = ls())

# loading packages
library(data.table)
library(stargazer)

# I need the 64 bit package or my office computer refuses to run some of this script
#install.packages('bit64')
library(bit64)

# loading NRA data for later calculations
load("NRAclean.RData")
# loading price data


# loading in the data I need
standardBase <- fread("SimulationResults/CountryValues_standard_bl.csv")
summary(standardBase)
# this is not specific enough, I need a by crop breakdown
# the ijk values should have what is needed

# does GDP 2009 equal model GDP? but the OG term is often negative
# according to the matlab notes: Crop Value = Y_i - V_OG, OG term being labor?
# so Y_i, model GDP, = V_OG + Crop Value
standardBase[, Yi := `OG term` + `Crop value`]
# how close is this to 2009 GDP?
standardBase[, Yi /`GDP (raw)`] # it is a tiny fraction
# which might explain why my resutls look this way

standardDetailed <- fread("SimulationResults/IJKvalues_standard_bl.csv")
summary(standardDetailed)
# this has what crop on a country by country base
# does this have an autoconsumption component?
standardDetailed[`Producer Name` == `Importer Name`] # yes, this has autoconsumption
# I belive this is in tons? but need to check at some point
# I need to pull in FAOstat data on prices, which I have somewhere for regressions
# and also the nominal rate of assistance, at least to calculate baseline values for comparison
# and the fall in expenditure
# actually I already have raw prices here? which should work
# but those aren't undistored, using equilibrium prices w/ NRA instead

preWTOdetailed <- fread("SimulationResults/IJKvalues_1986_bl.csv")


freeTradeBase <- fread("SimulationResults/CountryValues_freeTrade_bl.csv")
summary(freeTradeBase)

freeTradeDetailed <- fread("SimulationResults/IJKvalues_freeTrade_bl.csv")
summary(freeTradeDetailed)

# do I have easy matches here?
intersect(standardDetailed$`Producer Name`, NRA$country)
# 43 matches which is what I think I had earlier with the tables?
intersect(standardDetailed$`Crop Name`, NRA$prod2)

NRAbase <- NRA[year == "2009"]
NRAbase[, `:=` (ccode = NULL, year = NULL)]
names(NRAbase) <- c("Producer Name", "Crop Name", "Producer NRA")

# recreating this with pre-WTO values
NRApreWTO <- NRA[year == "1986"]
NRApreWTO[, `:=` (ccode = NULL, year = NULL)]
names(NRApreWTO) <- c("Producer Name", "Crop Name", "Producer NRA")


# merging in for producers first of all, I think the concern here is finding the amount
# paid to consumers in foreign countries?
standardDetailed <- merge(standardDetailed, NRAbase, all.x = TRUE) # need to make sure to keep the detailed observations
# filling NAs with 0s to produce a lower bound for this
standardDetailed[is.na(`Producer NRA`)]$`Producer NRA` <- 0 # I think the default is just 0?

# repeating with 1986 simulation, mergining in and filling NAs
preWTOdetailed <- merge(preWTOdetailed, NRApreWTO, all.x = TRUE)
names(NRApreWTO) <- c("Importer Name", "Crop Name", "Importer NRA")
preWTOdetailed <- merge(preWTOdetailed, NRApreWTO, all.x = TRUE, by = intersect(names(preWTOdetailed), names(NRApreWTO)))
preWTOdetailed[is.na(`Importer NRA`)]$`Importer NRA` <- 0
preWTOdetailed[is.na(`Producer NRA`)]$`Producer NRA` <- 0

names(NRAbase) <- c("Importer Name", "Crop Name", "Importer NRA")
standardDetailed <- merge(standardDetailed, NRAbase, all.x = TRUE, by = intersect(names(standardDetailed), names(NRAbase)))
# be sure to use intersect and not interact
standardDetailed[is.na(`Importer NRA`)]$`Importer NRA` <- 0

# so first lets find gross expenditure from the producing countries' perspective
standardDetailed[, ProducerIndividualCropGSE := (`Producer price (equilibrium)`/(1+`Producer NRA`))*Demand_ijk*`Producer NRA`]
# so if tons holds up this is an average of 1 million but a max of 100 billion
# or actually this is just per crop
# I'm using the wrong price, this needs to be an undistored price, so the values
# under free trade?  or modified by the NRA
# what 'undistorted prices' is is not specified by Anderson so I'll just divide out the NRA
# needs to be 1 + NRA for division as the baseline is 0
standardDetailed[, ProducerGSE := sum(ProducerIndividualCropGSE), by = "Producer Name"]
# average of 50 million, max of 200 billion, so most countries aren't spending that much
# which will be compounded by this change

# now what if I want the change from the exporter's perspective?
# for free trade to the base I can just use the total GSE? as it falls to 0 anyway
# but it needs to be for exports specifically, autoconsumption is not currently excluded
standardDetailed[ `Producer Name` != `Importer Name` , 
                  ProducerExportGSE := sum(ProducerIndividualCropGSE), by = "Producer Name"]
# so yes this reflects actual export support

# repeating this with 1986 data
preWTOdetailed[, ProducerIndividualCropGSE := (`Producer price (equilibrium)`/(1+`Producer NRA`))*Demand_ijk*`Producer NRA`]
preWTOdetailed[, ProducerGSE := sum(ProducerIndividualCropGSE), by = "Producer Name"]
preWTOdetailed[ `Producer Name` != `Importer Name` , 
                  ProducerExportGSE := sum(ProducerIndividualCropGSE), by = "Producer Name"]

# need to actually make importer GSE
standardDetailed[, ImporterIndividualCropGSE := (`Producer price (equilibrium)`/(1+`Importer NRA`))*Demand_ijk*`Importer NRA`]
# need to check my math on the above

standardDetailed[, ImporterGSE := sum(ImporterIndividualCropGSE, na.rm = TRUE), by = "Importer Name"]


# making a total GSE measure, which might be accurate?  need to think about this
standardDetailed[`Producer Name` == `Importer Name` ,`Total GSE` := ProducerGSE + ImporterGSE]
# I'm getting 2100 unique combinations here, where is that starting?
unique(standardDetailed$ProducerGSE) # not here
unique(standardDetailed$ImporterGSE) # not here
unique(standardDetailed$`Total GSE`) # this is no longer gives extra obs but I think the sums aren't accurate?
# checking this later on where I make y, the problem is the suite of countries is doubled, with half of them just being NAs
# it's not just the same values so the 2nd set may be valid sums?  but I don't know why this is happening
# only 43 unique values for total GSE

# again with 1986 data
preWTOdetailed[, ImporterIndividualCropGSE := (`Producer price (equilibrium)`/(1+`Importer NRA`))*Demand_ijk*`Importer NRA`]
preWTOdetailed[, ImporterGSE := sum(ImporterIndividualCropGSE, na.rm = TRUE), by = "Importer Name"]


# making a total GSE measure, which might be accurate?  need to think about this
standardDetailed[`Producer Name` == `Importer Name` ,`Total GSE` := ProducerGSE + ImporterGSE]


# If I assume that all barriers are set to 0 for free trade, it's just the reversal of that
# expenditure?
# can I pull out those unique values?
unique(standardDetailed[ , list(`Producer Name` , ProducerGSE)])
x <- unique(standardDetailed[ , list(`Producer Name` , ProducerExportGSE)])
# these are much larger than the welfare gains/losses
# but are maybe not specific enough?  This is just from the producer perspective
# need to go back to schmitz's work on how this should look?

# exporter payments are high variance, some nations will gain a lot from this,
# others are losing a lot of revenue, also a lot of NAs

# for the amount of loss based on subsidy driven exports I need to look at
# falls in exports when moving to free trade, caused by dropping the NRA, or
# coinciding with it
# so I need to find countries where exporting falls from standard to free trade
# and that have a positive NRA, but I need to look at this on a net basis
# will need to check my figures on this, and probably try to isolate which countries reduced exporting/moved out of a market
# in free trade, and that's subsidies paid to foreign consumers?
# these should be ordered the same?
standardDetailed$`Free Trade Demand` <- freeTradeDetailed$Demand_ijk
# I don't actually need this for free trade?  but I will need it for the 1986
# counterfactual

# free trade welfare changes
freeTradeBase[ , list(`Country name`, `Welfare numerator`)] # I specifically need
# to recreate the welfare change from standard base

# so merging freeTrade into standard, or just filling in the column?
# followed by the same with the GSE changes
standardBase$`Free Trade Welfare` <- freeTradeBase$`Welfare numerator`
standardBase[, `Free Trade Welfare Change` := `Free Trade Welfare` - `Welfare numerator`]

# taking the GSE here, need to get rid of NAs first and alphabetizing
x <- setorder( x[ !is.na(ProducerExportGSE)], `Producer Name`)

# is this method wrong? just filling in the 42 countries with fillin in obs?
standardBase$`Export Support` <- x$ProducerExportGSE
# making the measure
# need to look at discerning how much of this stays domestic via producers
standardBase[, `Welfare Less Leakage` := ((`Free Trade Welfare Change` + `Export Support`)/`GDP (raw)`)*100]
summary(standardBase) # again the median is negative, and the magnitude is increased

standardBase[, list(`Country name`, `Welfare Less Leakage`)]
# huge gains in Burma, 6% of GDP, also in Ghana, Colombia; massive losses in Uganda
NRAbase[`Importer Name` == "Uganda"] # Uganda relies on Coffee, maize, rice, tea taxes
# for revenue
# should I cut the negatives out of this?  since if they're raising revenue that effect
# is unclear
# is RAW GDP a valid basis for comparison? unclear
standardBase[`Export Support` >= 0 , `Export Support, No Revenue Taxes` := `Export Support`]
standardBase[, `Welfare Less Leakage, No Revenue Taxes` := 
               ((`Free Trade Welfare Change` + `Export Support, No Revenue Taxes`)/Yi)*100]
# excluding countries that raise revenue from taxes on exports on net results in positive outcomes at the median
# unclear about how valid this formulation is
standardBase[, list(`Country name`, `Welfare Less Leakage, No Revenue Taxes`)]
# Columbia's value is implausibly high but Myanamar has 1/3 of GDP as agriculture
standardBase[, `Export GSE, Percent GDP` := (`Export Support`/Yi)*100]

# folding in total GSE here
y <- unique(standardDetailed[ , list(`Producer Name` , `Total GSE`)])
# should there be this many 0s/NAs?  why is the entire list doubled up?
NRA[country == "Argentina"]
# Algeria having 0 makes sense but why does Argentina have a NA? need to drop NA from sums earlier in the process
nrow(y[is.na(`Total GSE`)])


# making export GSE table
gseStandardTable <- standardBase[, list(`Country name`, `Export Support`, `Export GSE, Percent GDP`)]

x <- NRA[year == "2009"]
x <- x[, c("country", "nra", "prod2")]
# I lose all observations of the general rate by dropping NAs?
# general NRA is not calculated apparently, dead end
x <- x[is.na(nra) == FALSE]
x <- x[, .(MeanProtectionChange = -mean(nra)*100), by = country]
# need to actually do a weighted average
names(x) <- c("Country name", "Mean Protection")

# adding NRA to this table for context
# need to truncate some of these variables
gseStandardTable <- merge(gseStandardTable, x, by = intersect(names(gseStandardTable), names(x)))

# trimming percent GDP column
gseStandardTable$`Export GSE, Percent GDP` <- round(gseStandardTable$`Export GSE, Percent GDP`, 3)

# I need to actually order this
setorder(gseStandardTable, `Export Support`)
sum(gseStandardTable$`Export Support`) # total is actually negative with revenue collection?
sum(gseStandardTable[`Export Support` > 0]$`Export Support`)
# 4 billion paid out disregarding taxes


# need to change some names to more conventional styling
gseStandardTable[`Country name` %like% "Korea"]$`Country name` <- "South Korea"
gseStandardTable[`Country name` %like% "Iran"]$`Country name` <- "Iran"
gseStandardTable[`Country name` %like% "Tanzania"]$`Country name` <- "Tanzania"

# organizing this by support as a % of GDP

setorder(gseStandardTable, `Export GSE, Percent GDP`)

# I need to make a table of GSE
# need to limit trailing 0s here, 1 column is much too wide
stargazer(gseStandardTable, summary = FALSE, title = "Gross Subsidy Equivalents, Exports",
          column.labels = c("Country", "Real Expenditure", "Percent GDP", "Mean Protection (Percent)"), omit.table.layout = "=!",
          label = "exportGSE", font.size = "footnotesize", notes = "Protection is the wedge between
          domestic and undistorted border prices")
# if notes are too long it breaks everything
# notes = "The protection is the unweighted average change in size the wedge between domestic
#and border prices if all price-distorting policies are dropped.  Ie if the change is -100 then goods across
# the border.  The amounts shown are the expenditure to achieve the level of protection in 2009"

# looking at total GSE paid, can just make this with the producer GSE I already have?
sum(unique(standardDetailed$ProducerGSE)) # equivalent to $60 billion in expenditure
gseProducerTable <- unique(standardDetailed[, list(`Producer Name`, `ProducerGSE`)])


# include the NRA's here for context as well?  I still have x, just need to sort out names first
# fold into standardBase and then break out again, then add NRA average from x
 names(gseProducerTable) <- c("Country name", "Producer GSE")
 
 gseProducerTable <- merge(standardBase, gseProducerTable, by = intersect(names(standardBase), names(gseProducerTable)))
 gseProducerTable[, `Producer GSE, Percent GDP` := (`Producer GSE`/Yi)*100]
 
 # and then just what I need for the table
 gseProducerTable <- gseProducerTable[, list(`Country name`, `Producer GSE`, `Producer GSE, Percent GDP`)]
 # merging in x
 gseProducerTable <- merge(gseProducerTable, x, by = intersect(names(gseProducerTable), names(x)))
 
 
 # need to change some names to more conventional styling
 gseProducerTable[`Country name` %like% "Korea"]$`Country name` <- "South Korea"
 gseProducerTable[`Country name` %like% "Iran"]$`Country name` <- "Iran"
 gseProducerTable[`Country name` %like% "Tanzania"]$`Country name` <- "Tanzania"
 
 # organizing this by support as a % of GDP
 
 setorder(gseProducerTable, `Producer GSE, Percent GDP`)
 stargazer(gseProducerTable, summary = FALSE, title = "Gross Subsidy Equivalents, All Production",
           column.labels = c("Country", "Real Expenditure", "Percent GDP", "Mean Protection (Percent)"), omit.table.layout = "=!",
           label = "producerGSE", font.size = "scriptsize", notes = "Protection is the wedge between
          domestic and undistorted border prices")

 
# need GDP, is it here?
summary(standardDetailed)
summary(standardBase) # do they even use this raw GDP figure?


# making a by crop table just to have
# first the GSE
standardDetailed[, `Crop GSE` := sum(ProducerIndividualCropGSE), by = `Crop Name`]

# need total production value for context
standardDetailed[, `Crop Expenditure` := sum(`Demand_ijk` * `Producer price (equilibrium)`) , by = `Crop Name`]
standardDetailed[, `Crop GSE, Percent Value` := (`Crop GSE`/`Crop Expenditure`)*100 , by = `Crop Name`]


cropGSETable <- unique(standardDetailed[, list(`Crop Name`, `Crop GSE`, `Crop GSE, Percent Value`)])

names(cropGSETable) <- c("Crop Name", "Crop GSE", "Crop GSE, Percent Production Value")


stargazer(cropGSETable, summary = FALSE, title = "Gross Subsidy Equivalents, By Crop", omit.table.layout = "=!",
          column.labels = c("Crop", "Real Expenditure", "Percent Value", "Mean Protection (Percent)"),
          label = "cropGSE", font.size = "footnotesize", notes = "Protection is the wedge between
          domestic and undistorted border prices")

# for 1986 this needs to look at both change in demand & expenditure
# or is that already handled with how GSE is calculated? I think it's already
# handled with how GSE is calculated



# comparing GSE with counterfactual welfare  changes to see if that alters anything
preWTOBase <- fread("SimulationResults/CountryValues_1986_bl.csv")
#freeTradeBase <- fread("SimulationResults/CountryValues_freeTrade_bl.csv")
# I don't need these, already have the welfare changes in standard base

standardBase[, `Free Trade Change, Less Export GSE` := `Free Trade Welfare Change` + `Export Support`]
standardBase[, `Free Trade Change Less Export GSE, Percent` := (`Free Trade Change, Less Export GSE`/Yi)*100]

# recreating free trade % of GDP measure
standardBase[, `Free Trade Welfare Change, Percent` := (`Free Trade Welfare Change`/Yi)*100]

# this implies some countries are raising a substantial amount of revenue in the free trade case
freeTradeLessExportGSEtable <- standardBase[, .(`Country name`, `Free Trade Welfare Change, Percent`, `Free Trade Change Less Export GSE, Percent`)]
# will finish the table before concluding anything

# making world entry here, or I can just make it in standard Base? no isn't there


setorder(freeTradeLessExportGSEtable, `Free Trade Change Less Export GSE, Percent`)
freeTradeLessExportGSEtable
# I was subtracting instead of adding, which is wrong given this is free trade


names(freeTradeLessExportGSEtable) <- c("Country", "Free Trade", "Less GSE")

# making world entry
worldEntry <- data.table(`Country` = "World", `Free Trade` = 100*sum(standardBase$`Free Trade Welfare Change`)/sum(standardBase$Yi),
                         `Less GSE` = 100*sum(standardBase$`Free Trade Welfare Change` + standardBase$`Export Support`)/sum(standardBase$Yi))

# binding world entry in so it is first in the table
intersect(names(worldEntry), names(freeTradeLessExportGSEtable))
freeTradeLessExportGSEtable <- rbind(worldEntry, freeTradeLessExportGSEtable)


stargazer(freeTradeLessExportGSEtable, summary = FALSE, title = "Free Trade Welfare Changes, Less GSE on Exports", omit.table.layout = "=!",
          column.labels = c("Country", "Real Expenditure", "Percent GDP", "Mean Protection (Percent)"),
          label = "freeTradeGSE", font.size = "scriptsize", notes = "All figures as a percent of modeled GDP")

# making a plot of the above
# first need some earleir work
load("simulationWelfare.RData")

#loading in the standard welfare with a bunch of comparisons from the simulation
# welfare reader script and adding the GSE figures
# which is throwing a fit for no reason
simulationWelfare$`Free Trade Change Less Export GSE, Percent` <- as.numeric(0)
simulationWelfare[`Country name` != "World"]$`Free Trade Change Less Export GSE, Percent` <- standardBase$`Free Trade Change Less Export GSE, Percent`


freeTradeLessGSEplot <- ggplot(simulationWelfare[`Country name` != "Egypt" & `Country name` != "World"][`Free Trade Change Less Export GSE, Percent` > -50 & `Free Trade Change Less Export GSE, Percent` < 50],
                               aes(y = `Free Trade Change Less Export GSE, Percent`, x = `Average NRA`)) + geom_point(aes(color = Yi/`Population, 2009`), size = 3 ) +
        ylab("Percent GDP Change") + xlab("Average NRA") + guides(color=guide_legend(title="GDP Per Capita")) +
        ggtitle("Removal of All Barriers Welfare Change, with GSE Expenditures")



# WTO counterfactual, less export support spending
# I do apparently need the pre-WTO numbers because they aren't saved in standard base for some reason
preWTOBase$`Country name` == standardBase[`Country name` != "World"]$`Country name`
standardBase$`Pre-WTO Welfare` <- preWTOBase$`Welfare numerator`

# now have the preWTO data, just making the welfare change and then looking at GSE
standardBase[, `Pre-WTO Welfare Change` := `Pre-WTO Welfare` - `Welfare numerator`]
standardBase[, `Pre-WTO Welfare Change, Percent` := 100*`Pre-WTO Welfare Change`/Yi]

# pulling out the GSE numbers, all currently NA for some reason, na doubles
x <- unique(preWTOdetailed[is.na(ProducerExportGSE) == FALSE , list(`Producer Name` , ProducerExportGSE)])
setorder(x, `Producer Name`)
standardBase$`Pre-WTO Export Support` <- x$ProducerExportGSE
standardBase[, `Pre-WTO Export GSE, Percent` := 100*`Pre-WTO Export Support`/Yi]
# not reverting so this is a difference
standardBase[, `Pre-WTO Welfare Change Less GSE, Percent` := `Pre-WTO Welfare Change, Percent` -  `Pre-WTO Export GSE, Percent`]

# assembling table
preWTOgseTable <- standardBase[, .(`Country name`, `Pre-WTO Welfare Change, Percent`, `Pre-WTO Welfare Change Less GSE, Percent`)]
setorder(preWTOgseTable, `Pre-WTO Welfare Change Less GSE, Percent`)

# changing names for the table since stargazer doesn't take column titles when reporting
# full datasets
names(preWTOgseTable) <- c("Country", "Pre-WTO", "Less GSE")
        
        
# making world entry and binding
worldEntry <- data.table(`Country` = "World", `Pre-WTO` = 100*sum(standardBase$`Pre-WTO Welfare Change`)/sum(standardBase$Yi),
                         `Less GSE` = 100*sum(standardBase$`Pre-WTO Welfare Change` - standardBase$`Pre-WTO Export Support`)/sum(standardBase$Yi))

# binding world entry in so it is first in the table
preWTOgseTable <- rbind(worldEntry, preWTOgseTable)

# printing
stargazer(preWTOgseTable, summary = FALSE, title = "Pre-WTO Welfare Changes, Less GSE on Exports", omit.table.layout = "=!",
          column.labels = c("Country", "Real Expenditure", "Percent GDP"),
          label = "preWTOGSE", font.size = "scriptsize", notes = "All figures as a percent of modeled GDP")

# moving some variables
simulationWelfare$`Pre-WTO Welfare Change Less GSE, Percent` <- as.numeric(0)
simulationWelfare[`Country name` != "World"]$`Pre-WTO Welfare Change Less GSE, Percent` <- standardBase$`Pre-WTO Welfare Change Less GSE, Percent`


preWTOlessGSEplot <- ggplot(simulationWelfare[`Country name` != "Egypt" & `Country name` != "World"][`Pre-WTO Welfare Change Less GSE, Percent` < 50 & `Pre-WTO Welfare Change Less GSE, Percent` > -50],
                               aes(y = `Pre-WTO Welfare Change Less GSE, Percent`, x = `Average NRA`)) + geom_point(aes(color = Yi/`Population, 2009`), size = 3 ) +
        ylab("Percent GDP Change") + xlab("Average NRA") + guides(color=guide_legend(title="GDP Per Capita")) +
        ggtitle("Reversion to 1986 Welfare Change, Less Export GSE")


# making small tables for presentation
countriesOfInterest <- c("World",  "Germany" , "Ecuador"     , "France" , "Viet Nam"  ,
                         "Ethiopia" , "United States"   , "China"  ,   "Japan" , 
                         "South Korea"           ,           "Australia")

freeTradeSmallTable <- freeTradeLessExportGSEtable[Country %in% countriesOfInterest]
preWTOsmallTable <- preWTOgseTable[Country %in% countriesOfInterest]

stargazer(freeTradeSmallTable, summary = FALSE, title = "Free Trade Welfare Changes, Less GSE on Exports", omit.table.layout = "=!",
          column.labels = c("Country", "Real Expenditure", "Percent GDP"),
          label = "freeTradeGSEsmall", font.size = "scriptsize", notes = "All figures as a percent of modeled GDP")




stargazer(preWTOsmallTable, summary = FALSE, title = "Pre-WTO Welfare Changes, Less GSE on Exports", omit.table.layout = "=!",
          column.labels = c("Country", "Real Expenditure", "Percent GDP"),
          label = "preWTOGSEsmall", font.size = "scriptsize", notes = "All figures as a percent of modeled GDP")


